package jason.eventsourcing;

import jason.JasonException;
import jason.architecture.AgArch;
import jason.asSemantics.Circumstance;
import jason.control.ExecutionControl;
import jason.infra.local.BaseLocalMAS;
import jason.infra.local.LocalExecutionControl;
import jason.mas2j.ClassParameters;
import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EvtSrcLocalExecutionControl extends LocalExecutionControl {
    private static final Logger logger = Logger.getLogger(
            EvtSrcLocalExecutionControl.class.getName());

    public EvtSrcLocalExecutionControl(ClassParameters userControlClass,
                                       BaseLocalMAS masRunner) throws JasonException {
        setMasRunner(masRunner);
        try {
            setUserController((ExecutionControl) Class.forName(
                            userControlClass.getClassName())
                    .getConstructor()
                    .newInstance());
            getUserController().setExecutionControlInfraTier(this);
            getUserController().init(userControlClass.getParametersArray());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error ", e);
            throw new JasonException(
                    "The user execution control class instantiation '" + userControlClass + "' has failed!" + e.getMessage());
        }
    }

    @Override
    public Document getAgState(String agName) {
        AgArch arch = getMasRunner().getAg(agName).getFirstAgArch();

        // Arch doesn't exist for some reason?
        if (arch == null) {
            return null;
        }

        Document state = arch.getTS().getAg().getAgState();

        Circumstance C = arch.getTS().getC();

        return state;
    }

}
