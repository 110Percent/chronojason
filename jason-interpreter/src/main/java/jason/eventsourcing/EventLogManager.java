package jason.eventsourcing;

import jason.architecture.AgArch;
import jason.asSemantics.Agent;
import jason.asSemantics.Event;

import java.util.HashMap;

public class EventLogManager {
    private final HashMap<String, AgentEventLog> agentLogs;

    public EventLogManager() {
        agentLogs = new HashMap<>();
    }

    public void logEvent(AgArch agArch, int cycle, Event event) {
        String name = agArch.getAgName();
        if (!agentLogs.containsKey(name)) {
            agentLogs.put(name, new AgentEventLog(agArch));
        }
        agentLogs.get(name).logEvent(cycle, event);
    }

    public AgentEventLog getAgentLog(String agName) {
        return agentLogs.get(agName);
    }
}
