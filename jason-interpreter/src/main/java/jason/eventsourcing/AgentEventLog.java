package jason.eventsourcing;

import jason.architecture.AgArch;
import jason.asSemantics.Agent;
import jason.asSemantics.CircumstanceListener;
import jason.asSemantics.Event;
import jason.asSemantics.TransitionSystem;
import jason.infra.local.LocalAgArch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class AgentEventLog implements CircumstanceListener {
    private final AgArch agArch;
    private final HashMap<Integer, List<Event>> cycleLogs;
    private final TransitionSystem ts;

    public AgentEventLog(AgArch agArch) {
        this.agArch = agArch;
        this.ts = agArch.getTS();
        cycleLogs = new HashMap<>();
        cycleLogs.put(agArch.getCycleNumber(), new ArrayList<>());

    }

    public void logEvent(int cycle, Event event) {
        if (!cycleLogs.containsKey(cycle)) {
            cycleLogs.put(cycle, new ArrayList<>());
        }
        cycleLogs.get(cycle).add(event);
    }

    public void logEvent(Event event) {
        logEvent(agArch.getCycleNumber(), event);
    }

    public List<Event> getEvents(int cycle) {
        if (!cycleLogs.containsKey(cycle)) {
            return new ArrayList<>();
        }
        return cycleLogs.get(cycle);
    }

    public List<Event> getEvents() {
        return getEvents(agArch.getCycleNumber());
    }

    public void eventAdded(Event e) {
        ts.getLogger().fine(e.toString());
        logEvent(e);
    }
}
