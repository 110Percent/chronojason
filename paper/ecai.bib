@online{Fowler_2005,
  title           = "Event {Sourcing}",
  url             = {https://martinfowler.com/eaaDev/EventSourcing.html},
  journal         = {Martin Fowler},
  author          = {Fowler, Martin},
  year            = 2005,
  month           = {Dec}
}

@online{ChronoJason_Repo,
  title = "ChronoJason - Event Sourcing in Jason",
  author = {Davies, Curtis},
  year = 2024,
  url = {https://gitlab.com/110Percent/chronojason}
}

@article{Jason,
  title           = "A {J}ava-based interpreter for an extended version of
                  {AgentSpeak}",
  author          = {Bordini, Rafael H and H{\"u}bner, Jomi F},
  journal         = {University of Durham, Universidade Regional de Blumenau},
  volume          = 256,
  year            = 2007
}

@InProceedings{AgentSpeak,
  author          = "Rao, Anand S.",
  editor          = "Van de Velde, Walter and Perram, John W.",
  title           = "{AgentSpeak(L)}: {BDI} agents speak out in a logical
                  computable language",
  booktitle       = "Agents Breaking Away",
  year            = 1996,
  publisher       = "Springer Berlin Heidelberg",
  address         = "Berlin, Heidelberg",
  pages           = "42--55",
  abstract        = "Belief-Desire-Intention (BDI) agents have been investigated
                  by many researchers from both a theoretical specification
                  perspective and a practical design perspective. However, there
                  still remains a large gap between theory and practice. The
                  main reason for this has been the complexity of
                  theorem-proving or model-checking in these expressive
                  specification logics. Hence, the implemented BDI systems have
                  tended to use the three major attitudes as data structures,
                  rather than as modal operators. In this paper, we provide an
                  alternative formalization of BDI agents by providing an
                  operational and proof-theoretic semantics of a language
                  AgentSpeak(L). This language can be viewed as an abstraction
                  of one of the implemented BDI systems (i.e., PRS) and allows
                  agent programs to be written and interpreted in a manner
                  similar to that of horn-clause logic programs. We show how to
                  perform derivations in this logic using a simple example.
                  These derivations can then be used to prove the properties
                  satisfied by BDI agents.",
  isbn            = "978-3-540-49621-2"
}

@inproceedings{10.5555/3635637.3663023,
  author          = {Rodriguez, Sebastian and Thangarajah, John and Davey,
                  Andrew},
  title           = {Design Patterns for Explainable Agents (XAg)},
  year            = 2024,
  isbn            = 9798400704864,
  publisher       = {International Foundation for Autonomous Agents and
                  Multiagent Systems},
  address         = {Richland, SC},
  abstract        = {The ability to explain the behaviour of the AI systems is a
                  key aspect of building trust, especially for autonomous agent
                  systems - how does one trust an agent whose behaviour can not
                  be explained? In this work, we advocate the use of design
                  patterns for developing explainable-by-design agents (XAg), to
                  ensure explainability is an integral feature of agent systems
                  rather than an "add-on" feature. We present TriQPAN (Trigger,
                  Query, Process, Action and Notify), a design pattern for XAg.
                  TriQPAN can be used to explain behaviours of any agent
                  architecture and we show how this can be done to explain
                  decisions such as why the agent chose to pursue a particular
                  goal, why or why didn't the agent choose a particular plan to
                  achieve a goal, and so on. We term these queries as direct
                  queries. Our framework also supports temporal correlation
                  queries such as asking a search and rescue drone, "which
                  locations did you visit and why?". We implemented TriQPAN in
                  the SARL agent language, built-in to the goal reasoning
                  engine, affording developers XAg with minimal overhead. The
                  implementation will be made available for public use. We
                  describe that implementation and apply it to two case studies
                  illustrating the explanations produced, in practice.},
  booktitle       = {Proceedings of the 23rd International Conference on
                  Autonomous Agents and Multiagent Systems},
  pages           = {1621–1629},
  numpages        = 9,
  keywords        = {aose, design patterns, emas, explainable agents},
  location        = {Auckland, New Zealand},
  series          = {AAMAS '24}
}

@inproceedings{AgentDialogue,
  title           = "Explaining BDI agent behaviour through dialogue",
  abstract        = "BDI agents act in response to external inputs and their
                  internal plan library. Understanding the root cause of BDI
                  agent action is often difficult, and in this paper we present
                  a dialogue based approach for explaining the behaviour of a
                  BDI agent. We consider two dialogue participants who may have
                  different views regarding the beliefs, plans and external
                  events which drove agent action (encoded via traces). These
                  participants make utterances which incrementally reveal their
                  traces to each other, allowing them to identify divergences in
                  the traces, or to conclude that the traces agree. In practice,
                  we envision a human taking on the role of a dialogue
                  participant, with the BDI agent itself acting as the other
                  participant. The dialogue then facilitates explanation,
                  understanding and debugging of BDI agent behaviour. After
                  presenting our formalism and its properties, we describe our
                  implementation of the system and provide an example of its use
                  in a simple scenario",
  keywords        = "Dialogues, BDI, Explanation",
  author          = "Dennis, {Louise A.} and Nir Oren",
  year            = 2021,
  language        = "English",
  publisher       = "International Foundation for Autonomous Agents and
                  Multiagent Systems (IFAAMAS)",
  pages           = "429--437",
  editor          = "U Endriss and A Nowe and F Dignum and A Lomuscio",
  booktitle       = "Proc. of the 20th International Conference on Autonomous
                  Agents and Multiagent Systems (AAMAS 2021)",
}

@InProceedings{ExplanationFramework,
  author          = "Ciatto, Giovanni and Schumacher, Michael I. and Omicini,
                  Andrea and Calvaresi, Davide",
  editor          = "Calvaresi, Davide and Najjar, Amro and Winikoff, Michael
                  and Fr{\"a}mling, Kary",
  title           = "Agent-Based Explanations in AI: Towards an Abstract
                  Framework",
  booktitle       = "Explainable, Transparent Autonomous Agents and Multi-Agent
                  Systems",
  year            = 2020,
  publisher       = "Springer International Publishing",
  address         = "Cham",
  pages           = "3--20",
  abstract        = "Recently, the eXplainable AI (XAI) research community has
                  focused on developing methods making Machine Learning (ML)
                  predictors more interpretable and explainable. Unfortunately,
                  researchers are struggling to converge towards an unambiguous
                  definition of notions such as interpretation, or,
                  explanation---which are often (and mistakenly) used
                  interchangeably. Furthermore, despite the sound metaphors that
                  Multi-Agent System (MAS) could easily provide to address such
                  a challenge, and agent-oriented perspective on the topic is
                  still missing. Thus, this paper proposes an abstract and
                  formal framework for XAI-based MAS, reconciling notions, and
                  results from the literature.",
  isbn            = "978-3-030-51924-7"
}

@article{DARPAXAI,
  author          = "{Turek, M}",
  title           = "{Explainable Artificial Intelligence} {(XAI)}",
  url             =
                  {https://www.darpa.mil/program/explainable-artificial-intelligence},
  journal         = "Defense Advanced Research Projects Agency"
}

@article{Kafka,
  author          = "{Apache Software Foundation}",
  title           = "{Apache Kafka}",
  url             = "https://kafka.apache.org/"
}

@book{MultiAgentSystemsBook,
  title           = "{Programming Multi-Agent Systems in AgentSpeak using
                  Jason}",
  author          = "Rafael H. Bordini and Jomi Fred Hübner",
  editor          = "Michael Wooldridge",
  publisher       = "Wiley",
  address         = "West Sussex, England",
  year            = 2007
}

@InProceedings{DebuggingIsExplaining,
  author          = "Hindriks, Koen V.",
  editor          = "Rahwan, Iyad and Wobcke, Wayne and Sen, Sandip and
                  Sugawara, Toshiharu",
  title           = "Debugging Is Explaining",
  booktitle       = "PRIMA 2012: Principles and Practice of Multi-Agent Systems",
  year            = 2012,
  publisher       = "Springer Berlin Heidelberg",
  address         = "Berlin, Heidelberg",
  pages           = "31--45",
  abstract        = "Debugging is a process of finding and reducing the number
                  of bugs, or defects, in a program. A defect typically is
                  detected because the program generates unexpected behaviour.
                  In order to locate the cause of a defect, it therefore is
                  essential to explain why this behaviour is generated. In this
                  paper, we propose a new debugging approach for agent-oriented
                  programming that exploits the structure and basic concepts
                  that are used in logic-based agent programs for providing
                  answers to some of such why questions. Our approach is based
                  on the fact that the behaviour of an agent-oriented program
                  can be traced back to basically two sources. First, agents
                  derive their choice of action from their beliefs and goals -
                  two fundamental programming concepts in agent-oriented
                  programming. These folk psychological concepts provide reasons
                  for doing something. Second, agent programs are rule-based
                  programs and the evaluation of rules to a large extent
                  determines the program's behaviour.",
  isbn            = "978-3-642-32729-2"
}

@article{TemporalArgumentation,
  author          = {Pardo, Pere and Godo, Lluís},
  title           = "{A temporal argumentation approach to cooperative planning
                  using dialogues}",
  journal         = {Journal of Logic and Computation},
  volume          = 28,
  number          = 3,
  pages           = {551-580},
  year            = 2015,
  month           = 02,
  abstract        = "{In this article, we study a dialogue-based approach to
                  multi-agent collaborative plan search in the framework of
                  t-DeLP, an extension of DeLP for defeasible temporal
                  reasoning. In t-DeLP programs, temporal facts and rules
                  combine into arguments, which compare against each other to
                  decide which of their conclusions are to prevail. By adding
                  temporal actions for multiple agents to this argumentative
                  logic programming framework, one obtains a centralized
                  planning framework. In this planning system, it can be shown
                  that breadth-first search is sound and complete for both
                  forward and backward planning. The main contribution is to
                  extend these results in centralized planning to cooperative
                  planning tasks, where the executing agents themselves are
                  assumed to have reasoning and planning abilities. In
                  particular, we propose a planning algorithm where agents
                  exchange information on plans using suitable dialogues. We
                  show that the soundness and completeness properties of
                  centralized t-DeLP plan search are preserved, so the
                  dialoguing agents will reach an agreement upon a joint plan if
                  and only if some solution exists.}",
  issn            = {0955-792X},
  doi             = {10.1093/logcom/exv007},
  url             = {https://doi.org/10.1093/logcom/exv007},
  eprint          =
                  {https://academic.oup.com/logcom/article-pdf/28/3/551/24671906/exv007.pdf},
}

@InProceedings{tDeLP,
  author          = "Pardo, Pere and Godo, Llu{\'i}s",
  editor          = "Benferhat, Salem and Grant, John",
  title           = "t-DeLP: A Temporal Extension of the Defeasible Logic
                  Programming Argumentative Framework",
  booktitle       = "Scalable Uncertainty Management",
  year            = 2011,
  publisher       = "Springer Berlin Heidelberg",
  address         = "Berlin, Heidelberg",
  pages           = "489--503",
  abstract        = "The aim of this paper is to offer an argumentation-based
                  defeasible logic that enables temporal forward reasoning. We
                  extend the DeLP logical framework by associating temporal
                  parameters to literals. A temporal logic program is a set of
                  temporal literals and durative rules. These temporal facts and
                  rules combine into durative arguments representing temporal
                  processes, that permit us to reason defeasibly about future
                  states. The corresponding notion of logical consequence, or
                  warrant, is defined slightly different from that of DeLP, due
                  to the temporal aspects. As usual, this notion takes care of
                  inconsistencies, and in particular we prove the consistency of
                  any logical program whose strict part is consistent. Finally,
                  we define and study a sub-class of arguments that seem
                  appropriate to reason with natural processes, and suggest a
                  modification to the framework that is equivalent to
                  restricting the logic to this class of arguments.",
  isbn            = "978-3-642-23963-2"
}

@INPROCEEDINGS{ExplainableDesignEval,
  author          = {Harbers, Maaike and van den Bosch, Karel and Meyer,
                  John-Jules},
  booktitle       = {2010 IEEE/WIC/ACM International Conference on Web
                  Intelligence and Intelligent Agent Technology},
  title           = {Design and Evaluation of Explainable BDI Agents},
  year            = 2010,
  volume          = 2,
  pages           = {125-132},
  keywords        = {Training;Humans;Intelligent
                  agent;History;Guidelines;Artificial intelligence;Algorithm
                  design and analysis;Explanation;BDI agent;virtual training},
  doi             = {10.1109/WI-IAT.2010.115}
}

@inproceedings{BuildingExplainableAI,
  author          = {Core, Mark and Lane, H. and Lent, Michael and Gomboc, Dave
                  and Solomon, Steve and Rosenberg, Milton},
  year            = 2006,
  month           = 01,
  title           = {Building Explainable Artificial Intelligence Systems.}
}

@inproceedings{10.5555/3635637.3663023,
  author          = {Rodriguez, Sebastian and Thangarajah, John and Davey,
                  Andrew},
  title           = {Design Patterns for Explainable Agents (XAg)},
  year            = 2024,
  isbn            = 9798400704864,
  publisher       = {International Foundation for Autonomous Agents and
                  Multiagent Systems},
  address         = {Richland, SC},
  abstract        = {The ability to explain the behaviour of the AI systems is a
                  key aspect of building trust, especially for autonomous agent
                  systems - how does one trust an agent whose behaviour can not
                  be explained? In this work, we advocate the use of design
                  patterns for developing explainable-by-design agents (XAg), to
                  ensure explainability is an integral feature of agent systems
                  rather than an "add-on" feature. We present TriQPAN (Trigger,
                  Query, Process, Action and Notify), a design pattern for XAg.
                  TriQPAN can be used to explain behaviours of any agent
                  architecture and we show how this can be done to explain
                  decisions such as why the agent chose to pursue a particular
                  goal, why or why didn't the agent choose a particular plan to
                  achieve a goal, and so on. We term these queries as direct
                  queries. Our framework also supports temporal correlation
                  queries such as asking a search and rescue drone, "which
                  locations did you visit and why?". We implemented TriQPAN in
                  the SARL agent language, built-in to the goal reasoning
                  engine, affording developers XAg with minimal overhead. The
                  implementation will be made available for public use. We
                  describe that implementation and apply it to two case studies
                  illustrating the explanations produced, in practice.},
  booktitle       = {Proceedings of the 23rd International Conference on
                  Autonomous Agents and Multiagent Systems},
  pages           = {1621–1629},
  numpages        = 9,
  keywords        = {aose, design patterns, emas, explainable agents},
  location        = {<conf-loc>, <city>Auckland</city>, <country>New
                  Zealand</country>, </conf-loc>},
  series          = {AAMAS '24}
}

@inproceedings{Whyline2008,
  author          = {Ko, Amy J. and Myers, Brad A.},
  title           = {Debugging reinvented: asking and answering why and why not
                  questions about program behavior},
  year            = 2008,
  isbn            = 9781605580791,
  publisher       = {Association for Computing Machinery},
  address         = {New York, NY, USA},
  url             = {https://doi.org/10.1145/1368088.1368130},
  doi             = {10.1145/1368088.1368130},
  abstract        = {When software developers want to understand the reason for
                  a program's behavior, they must translate their questions
                  about the behavior into a series of questions about code,
                  speculating about the causes in the process. The Whyline is a
                  new kind of debugging tool that avoids such speculation by
                  instead enabling developers to select a question about program
                  output from a set of why did and why didn't questions derived
                  from the program's code and execution. The tool then finds one
                  or more possible explanations for the output in question,
                  using a combination of static and dynamic slicing, precise
                  call graphs, and new algorithms for determining potential
                  sources of values and explanations for why a line of code was
                  not reached. Evaluations of the tool on one task showed that
                  novice programmers with the Whyline were twice as fast as
                  expert programmers without it. The tool has the potential to
                  simplify debugging in many software development contexts.},
  booktitle       = {Proceedings of the 30th International Conference on
                  Software Engineering},
  pages           = {301–310},
  numpages        = 10,
  keywords        = {whyline},
  location        = {Leipzig, Germany},
  series          = {ICSE '08}
}

@misc{AIHLEG_2019,
  title           = {Ethics Guidelines for Trustworthy AI},
  url             =
                  {https://digital-strategy.ec.europa.eu/en/library/ethics-guidelines-trustworthy-ai},
  journal         = {Shaping Europe’s Digital Future},
  publisher       = {European Commission},
  author          = {High-Level Expert Group on Artificial Intelligence},
  year            = 2019,
  month           = {Apr}
}

@online{JasonRepo,
  title           = {Jason {G}it{H}ub Repository},
  author          = {Jason Developers},
  url    = {https://github.com/jason-lang/jason},
  year            = 2016
}

@article{LONGO2024102301,
  title           = {Explainable Artificial Intelligence (XAI) 2.0: A manifesto
                  of open challenges and interdisciplinary research directions},
  journal         = {Information Fusion},
  volume          = 106,
  pages           = 102301,
  year            = 2024,
  issn            = {1566-2535},
  doi             = {https://doi.org/10.1016/j.inffus.2024.102301},
  url             =
                  {https://www.sciencedirect.com/science/article/pii/S1566253524000794},
  author          = {Luca Longo and Mario Brcic and Federico Cabitza and Jaesik
                  Choi and Roberto Confalonieri and Javier Del Ser and Riccardo
                  Guidotti and Yoichi Hayashi and Francisco Herrera and Andreas
                  Holzinger and Richard Jiang and Hassan Khosravi and Freddy
                  Lecue and Gianclaudio Malgieri and Andrés Páez and Wojciech
                  Samek and Johannes Schneider and Timo Speith and Simone
                  Stumpf},
  keywords        = {Explainable artificial intelligence, XAI, Interpretability,
                  Manifesto, Open challenges, Interdisciplinarity, Ethical AI,
                  Large language models, Trustworthy AI, Responsible AI,
                  Generative AI, Multi-faceted explanations, Concept-based
                  explanations, Causality, Actionable XAI, Falsifiability},
  abstract        = {Understanding black box models has become paramount as
                  systems based on opaque Artificial Intelligence (AI) continue
                  to flourish in diverse real-world applications. In response,
                  Explainable AI (XAI) has emerged as a field of research with
                  practical and ethical benefits across various domains. This
                  paper highlights the advancements in XAI and its application
                  in real-world scenarios and addresses the ongoing challenges
                  within XAI, emphasizing the need for broader perspectives and
                  collaborative efforts. We bring together experts from diverse
                  fields to identify open problems, striving to synchronize
                  research agendas and accelerate XAI in practical applications.
                  By fostering collaborative discussion and interdisciplinary
                  cooperation, we aim to propel XAI forward, contributing to its
                  continued success. We aim to develop a comprehensive proposal
                  for advancing XAI. To achieve this goal, we present a
                  manifesto of 28 open problems categorized into nine
                  categories. These challenges encapsulate the complexities and
                  nuances of XAI and offer a road map for future research. For
                  each problem, we provide promising research directions in the
                  hope of harnessing the collective intelligence of interested
                  stakeholders.}
}

@book{Martin_1983,
  place           = {London},
  title           = {Managing the Data Base Environment},
  publisher       = {Prentice-Hall International},
  author          = {Martin, James},
  year            = 1983
}

@online{EventStoreDB,
  title           = {{E}vent{S}tore{DB}},
  author          = {Event Store Ltd},
  url = {https://www.eventstore.com/eventstoredb}
}

@article{swartout1991explanations,
  title           = {Explanations in knowledge systems: Design for explainable
                  expert systems},
  author          = {Swartout, William and Paris, Cecile and Moore, Johanna},
  journal         = {IEEE Expert},
  volume          = 6,
  number          = 3,
  pages           = {58--64},
  year            = 1991,
  publisher       = {IEEE}
}

@inproceedings{Rao1995BDIAF,
  title           = {BDI Agents: From Theory to Practice},
  author          = {Anand Srinivasa Rao and Michael P. Georgeff},
  booktitle       = {International Conference on Multiagent Systems},
  year            = 1995,
}
