package event.sim;

import org.w3c.dom.Element;

public class PositionedLog {
    private final Element record;
    private final int index;

    public PositionedLog(Element record, int index) {
        this.record = record;
        this.index = index;
    }

    public Element getRecord() {
        return record;
    }

    public int getIndex() {
        return index;
    }
}
