package event.sim.exception;

import jason.asSemantics.Agent;

public class AgentStateMismatchException extends SimulatorException {
    public AgentStateMismatchException(String message, Agent ag) {
        super(message + "\n\nAgent circumstance dump:\n" + ag.getTS().getC().toString(), ag);
    }
}
