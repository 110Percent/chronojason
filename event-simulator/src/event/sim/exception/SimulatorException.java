package event.sim.exception;

import jason.asSemantics.Agent;

public class SimulatorException extends RuntimeException {
    public SimulatorException(String message, Agent ag) {
        super("Error in Agent " + ag.getTS().getAgArch().getAgName() + " during Cycle " + ag.getTS().getAgArch().getCycleNumber() + ":\n" + message);
    }
}
