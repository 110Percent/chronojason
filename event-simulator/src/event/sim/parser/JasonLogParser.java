package event.sim.parser;

import event.sim.PositionedLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;

public class JasonLogParser {
    private NodeList records = null;
    private final String logPath;

    public JasonLogParser(String logPath) {
        System.out.println("JasonLogParser created");
        this.logPath = logPath;
        // Check if logPath is a valid path
        if (isValidLogPath(logPath)) {
            loadLog();
        } else {
            throw new RuntimeException("Invalid log path: " + logPath);
        }
    }

    private void loadLog() {
        // Load log file
        File logFile = new File(logPath);
        System.out.println("Loading log file: " + logFile);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setValidating(false);
        try {
            dbFactory.setFeature("http://xml.org/sax/features/validation", false);
            dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }

        Document logDocument;
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            logDocument = dBuilder.parse(logPath);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // logDocument.getDocumentElement().normalize();

        Element logRootElement = logDocument.getDocumentElement();
        records = logRootElement.getElementsByTagName("record");
    }

    public PositionedLog getNextAgentRecord(String agentName, int recordIndex) {
        int index = recordIndex;
        while (index < records.getLength()) {
            index++;
            Element agentRecord = (Element) records.item(index);
            if (agentRecord == null) {
                return null;
            }
            String logger = agentRecord.getElementsByTagName("logger")
                    .item(0)
                    .getTextContent();
            String logAgName = logger.split("\\.")[logger.split(
                    "\\.").length - 1];
            if (logAgName.equals(agentName)) {
                return new PositionedLog(agentRecord, index);
            }
        }
        return null;
    }

    private boolean isValidLogPath(String logPath) {
        File logFile = new File(logPath);
        return logFile.exists() && !logFile.isDirectory();
    }
}
