package event.sim.parser;

import java.util.List;

public class ParsedLogInstance {
    String methodName;
    List<String> arguments;

    public ParsedLogInstance(String methodName, List<String> arguments) {
        this.methodName = methodName;
        this.arguments = List.copyOf(arguments);
    }

    public String getMethodName() {
        return methodName;
    }

    public List<String> getArguments() {
        return arguments;
    }
}
