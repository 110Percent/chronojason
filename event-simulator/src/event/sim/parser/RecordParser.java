package event.sim.parser;

import event.sim.Simulator;
import org.w3c.dom.Element;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;

public class RecordParser {
    private static final Logger LOGGER = Logger.getLogger(RecordParser.class.getName());

    public static ParsedLogInstance parseRecord(Element record) {
        LOGGER.info("Parsing record " + record.getElementsByTagName("sequence").item(0).getTextContent() + " (" + record.getElementsByTagName("method").item(0).getTextContent() + ")");
        switch (record.getElementsByTagName("method").item(0).getTextContent()) {
            case (MethodName.PARSE_AGENTSPEAK_FILE) -> {
                return parseAgentSpeakFile(record);
            }
            case (MethodName.BELIEF_REVISION) -> {
                LOGGER.info("Belief revision");
                // TODO: Check if this is necessary to implement
                return null;
            }
            case (MethodName.UPDATE_EVENTS) -> {
                return parseUpdateEvents(record);
            }
            case (MethodName.SENSE) -> {
                return parseSense(record);
            }
            case (MethodName.PERCEIVE) -> {
                return parsePerceive(record);
            }
            case (MethodName.APPLY_SELEV) -> {
                return parseApplySelEv(record);
            }
            case (MethodName.DELIBERATE) -> {
                return new ParsedLogInstance(MethodName.DELIBERATE, List.of());
            }
            case (MethodName.APPLY_SELAPPL) -> {
                return parseApplySelAppl(record);
            }
            case (MethodName.APPLY_SELINT) -> {
                return parseApplySelInt(record);
            }
            case (MethodName.APPLY_PROCACT) -> {
                return parseApplyProcAct(record);
            }
            case (MethodName.APPLY_ADDIM) -> {
                return parseAddIM(record);
            }
            case (MethodName.APPLY_FINDOP) -> {
                return parseFindOp(record);
            }
            case (MethodName.APPLY_EXECINT) -> {
                return parseExecInt(record);
            }
            case (MethodName.ACT) -> {
                return parseAct(record);
            }
            case (MethodName.ACTION_EXECUTED) -> {
                return parseActionExecuted(record);
            }
            case (MethodName.APPLY_SEMANTIC_ACT) -> {
                return parseApplyClrInt(record);
            }
            case (MethodName.REASONING_CYCLE) -> {
                return parseReasoningCycle(record);
            }
            default -> {

            }
        }

        LOGGER.warning("Unknown method: " + record.getElementsByTagName("method").item(0).getTextContent() + "\n" + record.getElementsByTagName("message").item(0).getTextContent());
        return null;
    }

    private static ParsedLogInstance parseAgentSpeakFile(Element record) {
        Matcher matcher = LogRegex.PARSE_AS.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (!matcher.find()) {
            return null;
        }
        String asPath = matcher.group(1);
        LOGGER.info("AgentSpeak program path: " + asPath);
        if (asPath.startsWith("file:")) {
            asPath = asPath.substring(5);
            asPath = Simulator.logPath.getParent().toString() + File.separator + asPath;
        } else if (asPath.startsWith("jar:file:")) {
            // Ignore this; KQML plans automatically loaded if you include
            // the Jason jar in the classpath
            return null;
        } else {
            System.out.println("Unknown agent script path: " + asPath);
        }

        return new ParsedLogInstance(MethodName.PARSE_AGENTSPEAK_FILE, List.of(asPath));
    }

    private static ParsedLogInstance parseUpdateEvents(Element record) {
        Matcher matcher = LogRegex.PARSE_UPDATE_EVENTS.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (!matcher.find()) {
            return null;
        }
        String event = matcher.group(1);
        String events = matcher.group(2);
        LOGGER.info("Event: " + event);
        LOGGER.info("Events: " + events);
        return new ParsedLogInstance(MethodName.UPDATE_EVENTS, List.of(event, events));
    }

    private static ParsedLogInstance parseSense(Element record) {
        LOGGER.info("Parsing sense");
        return new ParsedLogInstance(MethodName.SENSE, List.of());
    }

    private static ParsedLogInstance parseApplySelEv(Element record) {
        LOGGER.info("Parsing applySelEv");
        LOGGER.info(record.getElementsByTagName("message").item(0).getTextContent());
        Matcher matcher = LogRegex.PARSE_APPLY_SELEV.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (!matcher.find()) {
            return null;
        }
        String selectedEvent = matcher.group(1);
        return new ParsedLogInstance(MethodName.APPLY_SELEV, List.of(selectedEvent));
    }

    private static ParsedLogInstance parsePerceive(Element record) {
        LOGGER.info("Parsing perceive");
        Matcher matcher = LogRegex.PARSE_PERCEIVE.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (!matcher.find()) {
            return null;
        }

        return new ParsedLogInstance(MethodName.PERCEIVE, List.of(matcher.group(1)));
    }

    private static ParsedLogInstance parseApplySelInt(Element record) {
        LOGGER.info("Parsing applySelInt");
        LOGGER.info("Message: " + record.getElementsByTagName("message").item(0).getTextContent());
        Matcher matcher = LogRegex.PARSE_APPLY_SELINT.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        Matcher nullMatcher = LogRegex.PARSE_NULL_SELINT.matcher(record.getElementsByTagName("message").item(0).getTextContent());

        if (nullMatcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_SELINT, List.of());
        }
        if (matcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_SELINT, List.of(matcher.group(1), matcher.group(2)));
        }

        return null;
    }

    private static ParsedLogInstance parseApplyProcAct(Element record) {
        Matcher matcher = LogRegex.PARSE_APPLY_PROCACT.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (!matcher.find()) {
            return null;
        }

        return new ParsedLogInstance(MethodName.APPLY_PROCACT, List.of());
    }

    private static ParsedLogInstance parseAddIM(Element record) {
        Matcher emptyMatcher = LogRegex.PARSE_APPLY_ADDIM_EMPTY.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        Matcher matcher = LogRegex.PARSE_APPLY_ADDIM.matcher(record.getElementsByTagName("message").item(0).getTextContent());

        if (emptyMatcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_ADDIM, List.of());
        }
        if (matcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_ADDIM, List.of(matcher.group(1)));
        }

        return null;
    }

    private static ParsedLogInstance parseFindOp(Element record) {
        Matcher matcher = LogRegex.PARSE_FINDOP.matcher(record.getElementsByTagName("message").item(0).getTextContent());

        Matcher relApplMatcher = LogRegex.PARSE_RELAPPLPLR2.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (matcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_FINDOP, List.of(matcher.group(1)));
        }

        if (relApplMatcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_FINDOP, List.of());
        }

        return null;
    }

    private static ParsedLogInstance parseExecInt(Element record) {
        Matcher matcher = LogRegex.PARSE_EXECINT.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (matcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_EXECINT, List.of());
        }

        return null;
    }

    private static ParsedLogInstance parseAct(Element record) {
        Matcher resetMatcher = LogRegex.PARSE_RESET_ACT.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (resetMatcher.find()) {
            return new ParsedLogInstance(MethodName.ACT, List.of());
        }

        Matcher actionCleanupMatcher = LogRegex.PARSE_ACTION_CLEANUP.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (actionCleanupMatcher.find()) {
            return new ParsedLogInstance(MethodName.ACT, List.of("cleanup"));
        }

        return null;
    }

    private static ParsedLogInstance parseActionExecuted(Element record) {
        Matcher matcher = LogRegex.PARSE_ACTION_EXECUTED.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (matcher.find()) {
            return new ParsedLogInstance(MethodName.ACTION_EXECUTED, List.of(matcher.group(1)));
        }

        return null;
    }

    private static ParsedLogInstance parseApplyClrInt(Element record) {
        Matcher matcher = LogRegex.PARSE_CLRINT.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (matcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_SEMANTIC_ACT, List.of());
        }

        return null;
    }

    private static ParsedLogInstance parseApplySelAppl(Element record) {
        Matcher matcher = LogRegex.PARSE_SELAPPL.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (matcher.find()) {
            return new ParsedLogInstance(MethodName.APPLY_SELAPPL, List.of(matcher.group(1), matcher.group(2)));
        }

        return null;
    }

    private static ParsedLogInstance parseReasoningCycle(Element record) {
        Matcher xmlMatcher = LogRegex.PARSE_CIRCUMSTANCE_XML.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (xmlMatcher.find()) {
            return new ParsedLogInstance(MethodName.REASONING_CYCLE, List.of("Circumstance", record.getElementsByTagName("message").item(0).getTextContent()));
        }

        Matcher matcher = LogRegex.PARSE_REASONING_CYCLE_NUM.matcher(record.getElementsByTagName("message").item(0).getTextContent());
        if (matcher.find()) {
            return new ParsedLogInstance(MethodName.REASONING_CYCLE, List.of(matcher.group(1)));
        }

        return null;
    }
}
