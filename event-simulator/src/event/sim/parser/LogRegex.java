package event.sim.parser;

import java.util.regex.Pattern;

public interface LogRegex {
    Pattern PARSE_AS = Pattern.compile("^as2j: AgentSpeak program '([^']+)' parsed " + "successfully!$");
    Pattern PARSE_UPDATE_EVENTS = Pattern.compile("^Added event (.+), events = (.+)$");
    Pattern PARSE_PERCEIVE = Pattern.compile("^percepts: \\[(.+)]$");
    Pattern PARSE_LIST = Pattern.compile("^\\[(.+)]$");
    Pattern PARSE_APPLY_SELEV = Pattern.compile("Selected event (.+)");

    Pattern PARSE_RESET_DELIBERATE = Pattern.compile("^Reset deliberate$");

    Pattern PARSE_SELAPPL = Pattern.compile("^Selected option (.+) for event (.+)$");

    Pattern PARSE_APPLY_SELINT = Pattern.compile("^Selected intention (.+): \\n\\s+([^\\n]+(?:\\n\\s+[^\\n]+)*)+$");
    Pattern PARSE_NULL_SELINT = Pattern.compile("^Null selected intention$");

    Pattern PARSE_APPLY_PROCACT = Pattern.compile("^Applying ProcAct$");

    Pattern PARSE_APPLY_ADDIM_EMPTY = Pattern.compile("^Pushing empty intention");
    Pattern PARSE_APPLY_ADDIM = Pattern.compile("^Pushing intention: ((?:.|\\s)+)");

    Pattern PARSE_FINDOP = Pattern.compile("^Selected option (.+)$");
    Pattern PARSE_RELAPPLPLR2 = Pattern.compile("^RelApplPlRule2$");

    Pattern PARSE_EXECINT = Pattern.compile("^Applying ExecInt$");

    Pattern PARSE_ACTION_EXECUTED = Pattern.compile("^Action executed: ((?:.|\\s)+)$");
    Pattern PARSE_RESET_ACT = Pattern.compile("^Resetting Act$");
    Pattern PARSE_ACTION_CLEANUP = Pattern.compile("^Start action cleanup$");

    Pattern PARSE_CLRINT = Pattern.compile("^Applying ClrInt$");

    Pattern PARSE_REASONING_CYCLE_NUM = Pattern.compile("^Reasoning cycle (\\d+) finished.$");

    Pattern PARSE_CIRCUMSTANCE_XML = Pattern.compile("^<circumstance>(?:.+|\\n)+</circumstance>$");
}
