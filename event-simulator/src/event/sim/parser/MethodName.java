package event.sim.parser;

public interface MethodName {
    String PARSE_AGENTSPEAK_FILE = "parseAS";
    String BELIEF_REVISION = "brf";
    String UPDATE_EVENTS = "updateEvents";
    String SENSE = "sense";
    String PERCEIVE = "perceive";
    String APPLY_SELEV = "applySelEv";

    String DELIBERATE = "deliberate";

    String APPLY_SELAPPL = "applySelAppl";
    String APPLY_SELINT = "applySelInt";

    String APPLY_PROCACT = "applyProcAct";
    String APPLY_ADDIM = "applyAddIM";

    String APPLY_FINDOP = "applyFindOp";

    String APPLY_EXECINT = "applyExecInt";

    String APPLY_SEMANTIC_ACT = "applySemanticRuleAct";

    String ACT = "act";
    String ACTION_EXECUTED = "actionExecuted";

    String REASONING_CYCLE = "reasoningCycle";
}
