package event.sim;

import jason.architecture.AgArch;
import jason.asSemantics.ActionExec;

public class SimAgArch extends AgArch {
    private SimAgArch env;

    public SimAgArch() {
        super();
    }

    public void setEnv(SimAgArch env) {
        this.env = env;
    }

    @Override
    public void act(ActionExec action) {
        action.setResult(true);
        this.getTS().getC().addFeedbackAction(action);
    }
}
