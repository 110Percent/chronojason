package event.sim;

import jason.asSyntax.Structure;
import jason.environment.Environment;

public class DummyEnvironment extends Environment {

    public DummyEnvironment() {
        super();
    }

    public void actionExecuted(String agName, Structure actTerm, boolean success, Object infraData) {

    }
}
