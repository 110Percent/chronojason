package event.sim;

import event.sim.parser.JasonLogParser;
import event.sim.parser.RecordParser;
import org.w3c.dom.Element;

public class AgentLogFeeder {
    private final JasonLogParser logParser;
    private final String agentName;
    private int currentRecordIndex = -1;

    public AgentLogFeeder(JasonLogParser logParser, String agentName) {
        this.logParser = logParser;
        this.agentName = agentName;
    }

    public Element getNextRecord() {
        PositionedLog log = logParser.getNextAgentRecord(agentName,
                currentRecordIndex);
        if (log != null) {
            currentRecordIndex = log.getIndex();
            return log.getRecord();
        }
        return null;
    }


}
