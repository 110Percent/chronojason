package event.sim;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.FINE);
        Logger.getAnonymousLogger().addHandler(consoleHandler);

        if (args.length < 2) {
            throw new RuntimeException("2 arguments required: logfile and agent name");
        }
        String logPath = args[0];
        String agentName = args[1];
        new Simulator(logPath, agentName);

        System.exit(0);
    }
}
