package event.sim;

import event.sim.exception.AgentStateMismatchException;
import event.sim.exception.SimulatorException;
import event.sim.parser.JasonLogParser;
import event.sim.parser.MethodName;
import event.sim.parser.ParsedLogInstance;
import event.sim.parser.RecordParser;
import event.sim.validation.ActionChecker;
import event.sim.validation.IntentionChecker;
import event.sim.validation.SemanticReturnCode;
import event.sim.validation.SemanticValidation;
import jason.JasonException;
import jason.asSemantics.*;
import jason.asSyntax.Literal;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Simulator {
    private static final Logger LOGGER = Logger.getLogger(Simulator.class.getName());
    public static Path logPath;
    private final JasonLogParser logParser;
    private final DummyEnvironment environment = new DummyEnvironment();
    private final Agent ag = new Agent();
    private final HashMap<String, ActionExec> actionCache = new HashMap<>();
    private String agentName = "";

    public Simulator(String logPath, String agentName) {
        LOGGER.info("Simulator created");
        Simulator.logPath = Path.of(logPath);
        this.agentName = agentName;

        logParser = new JasonLogParser(logPath);
        AgentLogFeeder agentLogFeeder = new AgentLogFeeder(logParser, this.agentName);
        TransitionSystem ts = new TransitionSystem(ag, null, null, new SimAgArch());
        ag.setTS(ts);
        ag.initAg();
        ts.getLogger().setLevel(Level.FINE);
        LOGGER.info("Set arch");

        // REQUIRED for SelEv to work, or else BB percepts don't get cleared
        ag.getTS().getC().addEventListener(new DummyCircumstanceListener());

        Element record = agentLogFeeder.getNextRecord();
        try {
            while (record != null) {
                ParsedLogInstance log = RecordParser.parseRecord(record);
                if (log == null) {
                    record = agentLogFeeder.getNextRecord();
                    continue;
                }
                switch (log.getMethodName()) {
                    case MethodName.PARSE_AGENTSPEAK_FILE -> {
                        parseAgentSpeakFile(log.getArguments().get(0));
                    }
                    case MethodName.UPDATE_EVENTS -> {
                        checkEventLists(log.getArguments().get(1));
                    }
                    case MethodName.SENSE -> {
                        LOGGER.info("Sensing");
                    }
                    case MethodName.PERCEIVE -> {
                        simulatePerceptions(log.getArguments().get(0));
                    }
                    case MethodName.APPLY_SELINT -> {
                        boolean isNullIntention = log.getArguments().isEmpty();
                        if (isNullIntention) {
                            applySelInt(isNullIntention, null, null);
                        } else {
                            applySelInt(isNullIntention, log.getArguments().get(0), log.getArguments().get(1));
                        }
                    }
                    case MethodName.APPLY_SELEV -> {
                        applySelEv(log.getArguments().get(0));
                    }
                    case MethodName.DELIBERATE -> {
                        applyDeliberate();
                    }
                    case MethodName.APPLY_PROCACT -> {
                        applyProcAct();
                    }
                    case MethodName.APPLY_ADDIM -> {
                        boolean emptyIntention = log.getArguments().isEmpty();
                        applyAddIM(emptyIntention, emptyIntention ? null : log.getArguments().get(0));
                    }
                    case MethodName.APPLY_FINDOP -> {
                        String contents = null;
                        if (!log.getArguments().isEmpty()) {
                            contents = log.getArguments().get(0).split("]")[1];
                        }
                        applyFindOp(contents);
                    }
                    case MethodName.APPLY_EXECINT -> {
                        LOGGER.info("Applying ExecInt");
                        try {
                            ag.getTS().applyExecInt();
                        } catch (JasonException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    case MethodName.APPLY_SEMANTIC_ACT -> {
                        LOGGER.info("Applying ClrInt");
                        try {
                            ag.getTS().applyClrInt(ag.getTS().getC().getSI());
                        } catch (JasonException e) {
                            throw new RuntimeException(e);
                        }
                        ActionExec action = ag.getTS().getC().getAction();
                        LOGGER.info("Action: " + action);
                    }
                    case MethodName.ACT -> {
                        if (log.getArguments().isEmpty()) {
                            LOGGER.info("RESETTING ACT");
                            ag.getTS().getC().resetAct();
                        }

                        if (!log.getArguments().isEmpty() && log.getArguments().get(0).equals("cleanup")) {
                            LOGGER.info("Action cleanup");
                            ActionExec action = ag.getTS().getC().getAction();
                            if (action == null) {
                                LOGGER.info("Null action - skipping");
                            } else {
                                ag.getTS().getC().addPendingAction(action);
                                actionCache.put(action.toString().replace("false>", "true>"), action);
                            }
                        }

                        LOGGER.info("Intentions: " + ag.getTS().getC().getRunningIntentions().toString());
                        LOGGER.info("Feedback actions: " + ag.getTS().getC().getFeedbackActions().toString());
                    }
                    case MethodName.ACTION_EXECUTED -> {
                        LOGGER.info("Action executed: " + log.getArguments().get(0));
                        LOGGER.info(actionCache.toString());
                        ActionExec action = getActionFromCache(log.getArguments().get(0));
                        if (action != null) {
                            LOGGER.info("Action found in cache. Adding to feedback actions.");
                            action.setResult(true);
                            ag.getTS().getC().addFeedbackAction(action);
                            removeActionFromCache(log.getArguments().get(0));
                        } else {
                            throw new SimulatorException("Action not found in cache: " + log.getArguments().get(0), ag);
                        }
                    }
                    case MethodName.REASONING_CYCLE -> {
                        if (log.getArguments().get(0).equals("Circumstance")) {
                            checkCircumstanceString(log.getArguments().get(1));
                        } else {
                            bumpReasoningCycleNumber(log.getArguments().get(0));
                        }
                    }
                    default -> {
                        throw new RuntimeException("Method not handled: " + log.getMethodName());
                    }
                }
                record = agentLogFeeder.getNextRecord();

            }

            LOGGER.info("Final BB: " + ag.getBB().toString());
            LOGGER.info("Final " + ag.getTS().getC().toString());
            LOGGER.info("!!!!! Simulation complete !!!!!\nAssuming you didn't mess up your validation code, the simulator works as expected. :D");
        } catch (RuntimeException e) {
            LOGGER.log(Level.SEVERE, "Critical Error", e);
            System.exit(1);
        }


    }

    private void parseAgentSpeakFile(String path) {
        LOGGER.info("Parsing AgentSpeak file at " + path);
        try {
            ag.loadInitialAS(path);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        LOGGER.info("Initial BB: " + ag.getBB().toString());
        LOGGER.info("Initial Events: " + ag.getTS().getC().getEvents().toString());
    }

    private void checkEventLists(String loggedEvents) {
        LOGGER.info("Updated events. Checking event list");

        String simulatedEvents = ag.getTS().getC().getEvents().toString();

        LOGGER.info("Logged events: " + loggedEvents);
        LOGGER.info("Simulated events: " + simulatedEvents);

        if (!loggedEvents.equals(simulatedEvents)) {
            LOGGER.warning("Event list mismatch!\n" + "Logged events: " + loggedEvents + "\n" + "Simulated events: " + simulatedEvents);
        } else {
            LOGGER.info("Event list OK!");
        }
    }

    private void simulatePerceptions(String perceptions) {
        LOGGER.info("Pre-perceive Events: " + ag.getTS().getC().getEvents().toString());
        LOGGER.info("Pre-perceive BB: " + ag.getBB());
        LOGGER.info("Perceiving " + perceptions);
        String[] sLiterals = perceptions.split(", ");
        Collection<Literal> perceivedLiterals = new ArrayList<>();
        for (String sLiteral : sLiterals) {
            perceivedLiterals.add(Literal.parseLiteral(sLiteral));
        }
        LOGGER.info("Perceived literals to update: " + perceivedLiterals);
        ag.buf(perceivedLiterals);
        try {
            ag.getTS().applyProcMsg();
        } catch (JasonException e) {
            throw new RuntimeException(e);
        }
        LOGGER.info("Post-perceive Events: " + ag.getTS().getC().getEvents().toString());
        LOGGER.info("Post-perceive BB: " + ag.getBB().toString());
    }

    private void applySelEv(String loggedEvent) {
        LOGGER.info("Applying SelEv");
        try {
            LOGGER.info(ag.getTS().getC().getEvents().toString());
            ag.getTS().applySelEv();
            Event selectedEvent = ag.getTS().getC().getSE();
            if (selectedEvent == null) {
                LOGGER.info("Selected event is null");
                if (loggedEvent != null) {
                    throw new AgentStateMismatchException("Selected event mismatch! Logged " + "event was " + loggedEvent + " when simulated event was " + "null!", ag);
                }
            } else if (!loggedEvent.equals(selectedEvent.toString().split("\n")[0])) {
                throw new AgentStateMismatchException("Selected event mismatch!\n" + "Logged " + "event: " + loggedEvent + "\n" + "Selected event: " + selectedEvent, ag);
            } else {
                LOGGER.info("SelEv match: " + selectedEvent);
            }

            // Verify against semantic rules
            SemanticReturnCode semantics = SemanticValidation.validSelEv(ag);
            if (semantics.success()) {
                LOGGER.info("SelEv is semantically valid");
            } else if (semantics == SemanticReturnCode.SELECTED_EVENT_PERSISTENCE) {
                LOGGER.warning("SE was not discarded from E");
            } else if (semantics == SemanticReturnCode.INVALID_STATE_TRANSITION) {
                LOGGER.warning("Invalid state transition - Current state is now " + ag.getTS().getStepAct());
            }
        } catch (JasonException e) {
            throw new RuntimeException(e);
        }
    }

    private void applyDeliberate() {
        LOGGER.info("Resetting deliberate");

        ag.getTS().getC().resetDeliberate();
    }

    private void applyFindOp(String loggedContents) {
        LOGGER.info("Applying FindOp");
        try {
            ag.getTS().applyFindOp();
        } catch (JasonException e) {
            throw new RuntimeException(e);
        }
        if (loggedContents != null) {
            Option simulatedSO = ag.getTS().getC().getSelectedOption();
            String simulatedContents = simulatedSO.toString().split("]")[1];
            if (simulatedContents.equals(loggedContents)) {
                LOGGER.info("Selected options match!\n" + simulatedContents);
            } else {
                throw new AgentStateMismatchException("Selected option mismatch!\nLogged: " + loggedContents + "\nSimulated: " + simulatedSO, ag);
            }
        }
    }

    private void applySelInt(boolean nullIntention, String loggedIntentionName, String loggedIntentionContent) throws AgentStateMismatchException {
        LOGGER.info("Applying SelInt");
        try {
            ag.getTS().applySelInt();
        } catch (JasonException e) {
            throw new RuntimeException(e);
        }
        Intention selectedIntention = ag.getTS().getC().getSI();
        if (nullIntention) {
            if (selectedIntention == null) {
                LOGGER.info("Selected Intention is null. (match)");
                return;
            } else {
                throw new AgentStateMismatchException("Selected intention mismatch! Logged intention is null, while " + "simulated intention is " + selectedIntention, ag);
            }
        }

        LOGGER.info("Logged intention: " + loggedIntentionName + ": " + loggedIntentionContent);
        LOGGER.info("Simulated intention: " + selectedIntention);
        if (selectedIntention == null) {
            if (loggedIntentionContent != null) {
                throw new AgentStateMismatchException("Selected intention mismatch! Logged " + "intention was " + loggedIntentionContent + " when simulated intention was null!", ag);
            }
        } else {
            String simulatedIntentionName = selectedIntention.toString().split("\n")[0].trim();
            String simulatedIntentionContent = selectedIntention.toString().substring(simulatedIntentionName.length()).trim();
            if (!IntentionChecker.intentionsEqual(loggedIntentionContent, simulatedIntentionContent)) {
                throw new AgentStateMismatchException("Selected intention mismatch!\n" + "Logged intention: " + loggedIntentionContent + "\n" + "Selected intention: " + simulatedIntentionContent, ag);
            } else {
                LOGGER.info("SelInt match! " + simulatedIntentionName + " " + simulatedIntentionContent);
            }
        }

        // Verify against semantic rules
        SemanticReturnCode semantics = SemanticValidation.validSelInt(ag);
        if (semantics.success()) {
            LOGGER.info("SelInt is semantically valid");
        } else if (semantics == SemanticReturnCode.INVALID_STATE_TRANSITION) {
            LOGGER.warning("Invalid state transition - Current state is now " + ag.getTS().getStepAct());
        }
    }

    private void applyProcAct() {
        LOGGER.info("Applying ProcAct");
        LOGGER.info(ag.getTS().getC().getFeedbackActions().toString());

        try {
            ag.getTS().applyProcAct();
        } catch (JasonException e) {
            throw new RuntimeException(e);
        }
        ActionExec action = ag.getTS().getC().getAction();
        LOGGER.info("Action: " + action);
        LOGGER.info("Intentions: " + ag.getTS().getC().getRunningIntentions().toString());
    }

    private void applyAddIM(boolean emptyIntention, String intentionContent) {
        LOGGER.info("Applying AddIM");
        if (emptyIntention) {
            LOGGER.info("Pushed empty intention");
        } else {
            LOGGER.info("Non-empty intention. Logged intention: " + intentionContent);
        }
        try {
            ag.getTS().applyAddIM();
        } catch (JasonException e) {
            throw new RuntimeException(e);
        }
        LOGGER.info("AI: " + ag.getTS().getC().getAI());
        LOGGER.info("I: " + ag.getTS().getC().getRunningIntentions());
    }

    private void bumpReasoningCycleNumber(String logMsg) {
        int loggedCycleNum = Integer.parseInt(logMsg);
        ag.getTS().getAgArch().setCycleNumber(loggedCycleNum + 1); // Set to next cycle
        LOGGER.info("Bumped reasoning cycle number to " + (loggedCycleNum + 1));
        LOGGER.info("Current circumstance: " + ag.getTS().getC().toString());
    }

    private ActionExec getActionFromCache(String action) {
        // Uses ActionChecker
        String matchingKey = actionCache.keySet().stream().filter(key -> ActionChecker.actionsEqual(key, action)).findFirst().orElse(null);
        if (matchingKey != null) {
            return actionCache.get(matchingKey);
        } else {
            return null;
        }
    }

    private void removeActionFromCache(String action) {
        String matchingKey = actionCache.keySet().stream().filter(key -> ActionChecker.actionsEqual(key, action)).findFirst().orElse(null);
        if (matchingKey != null) {
            actionCache.remove(matchingKey);
        }
    }

    private void checkCircumstanceString(String logCircumstance) {
        DocumentBuilder db = null;
        try {
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
        InputSource is = new InputSource(new StringReader(logCircumstance));
        Document doc = null;
        try {
            doc = db.parse(is);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Element c = doc.getDocumentElement();

        String events = c.getElementsByTagName("events").item(0).getTextContent().replaceAll("intention \\d+:", "intention x:");
        String simEvents = ag.getTS().getC().getEvents().toString().replaceAll("intention \\d+:", "intention x:");
        LOGGER.info("Logged events: " + events);
        if (events.length() == simEvents.length()) {
            LOGGER.info("Events match!");
        } else {
            throw new AgentStateMismatchException("Circumstance mismatch!\n" + "Logged events: " + events + "\n" + "Simulated events: " + simEvents, ag);
        }

        String intentions = c.getElementsByTagName("intentions").item(0).getTextContent().replaceAll("intention \\d+:", "intention x:");
        String simIntentions = ag.getTS().getC().getRunningIntentions().toString().replaceAll("intention \\d+:", "intention x:");
        LOGGER.info("Logged intentions: " + intentions);
        if (intentions.length() == simIntentions.length()) {
            LOGGER.info("Intentions match!");
        } else {
            throw new AgentStateMismatchException("Circumstance mismatch!\n" + "Logged intentions: " + intentions + "\n" + "Simulated intentions: " + simIntentions, ag);
        }

        String pendingActions = c.getElementsByTagName("pendingActions").item(0).getTextContent().replaceAll("\\d+", "X");
        String simPendingActions = ag.getTS().getC().getPendingActions().toString().replaceAll("\\d+", "X");
        LOGGER.info("Logged pending actions: " + pendingActions);
        if (pendingActions.length() == simPendingActions.length()) {
            LOGGER.info("Pending actions match!");
        } else {
            throw new AgentStateMismatchException("Circumstance mismatch!\n" + "Logged pending actions: " + pendingActions + "\n" + "Simulated pending actions: " + simPendingActions, ag);
        }
    }
}
