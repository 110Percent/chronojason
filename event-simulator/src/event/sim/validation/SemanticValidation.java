package event.sim.validation;

import jason.asSemantics.Agent;
import jason.asSemantics.Circumstance;
import jason.asSemantics.TransitionSystem;

public class SemanticValidation {
    public static SemanticReturnCode validSelEv(Agent agent) {
        // Check result of SelEv against the semantic rule
        Circumstance c = agent.getTS().getC();
        if (c.getEvents().contains(c.getSE())) {
            // Selected Event should not persist in event list
            return SemanticReturnCode.SELECTED_EVENT_PERSISTENCE;
        }

        TransitionSystem.State step = agent.getTS().getStepAct();
        if (step != TransitionSystem.State.RelPl && step != TransitionSystem.State.SelInt) {
            // Invalid state transition
            return SemanticReturnCode.INVALID_STATE_TRANSITION;
        }

        return SemanticReturnCode.SUCCESS;
    }

    public static SemanticReturnCode validSelInt(Agent agent) {
        // Check result of SelInt against the semantic rule
        Circumstance c = agent.getTS().getC();
        TransitionSystem.State step = agent.getTS().getStepAct();

        if (c.hasRunningIntention() || c.getSI() != null) {
            if (step != TransitionSystem.State.ExecInt) {
                return SemanticReturnCode.INVALID_STATE_TRANSITION;
            }
        } else {
            // Internal keyword for ProcMsg
            if (step != TransitionSystem.State.StartRC) {
                return SemanticReturnCode.INVALID_STATE_TRANSITION;
            }
        }

        return SemanticReturnCode.SUCCESS;
    }


}
