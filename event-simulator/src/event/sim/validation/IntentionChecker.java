package event.sim.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IntentionChecker {
    private static final Pattern IM_PATTERN = Pattern.compile("^(.+\\[.+] <- ... .*) / (\\{.*\\})$");

    public static boolean intentionsEqual(String intention1, String intention2) {
        String[] intentions1 = Arrays.stream(intention1.split("\n")).map(String::trim).toArray(String[]::new);
        String[] intentions2 = Arrays.stream(intention2.split("\n")).map(String::trim).toArray(String[]::new);

        if (intentions1.length != intentions2.length) {
            return false;
        }

        for (int i = 0; i < intentions1.length; i++) {
            Matcher matcher1 = IM_PATTERN.matcher(intentions1[i]);
            Matcher matcher2 = IM_PATTERN.matcher(intentions2[i]);

            if (matcher1.matches() && matcher2.matches()) {
                if (!matcher1.group(1).equals(matcher2.group(1))) {
                    throw new RuntimeException("Intention mismatch");
                }

                if (!variableStatesEqual(matcher1.group(2), matcher2.group(2))) {
                    return false;
                }
            } else {
                throw new RuntimeException("No regex match?\n" + intentions1[i] + "\n" + intentions2[i]);
            }

        }

        return true;
    }

    private static boolean variableStatesEqual(String variableState1, String variableState2) {
        String[] variables1 = variableState1.substring(1, variableState1.length() - 1).split(", ");
        String[] variables2 = variableState2.substring(1, variableState2.length() - 1).split(", ");
        if (variables1.length != variables2.length) {
            return false;
        }
        if (variables1[0].isBlank() && variables2[0].isBlank()) {
            return true;
        }

        HashMap<String, String> constantMap = new HashMap<>();
        ArrayList<String> variableValues = new ArrayList<>();

        for (int i = 0; i < variables1.length; i++) {
            String[] assignment = variables1[i].split("=");
            if (assignment[0].startsWith("_")) {
                variableValues.add(assignment[1]);
            } else {
                constantMap.put(assignment[0], assignment[1]);
            }
        }

        for (int i = 0; i < variables2.length; i++) {
            String[] assignment = variables2[i].split("=");
            if (assignment[0].startsWith("_")) {
                if (variableValues.contains(assignment[1])) {
                    variableValues.remove(assignment[1]);
                } else {
                    return false;
                }
            } else {
                if (constantMap.containsKey(assignment[0])) {
                    if (constantMap.get(assignment[0]).equals(assignment[1])) {
                        constantMap.remove(assignment[0]);
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }

        return true;
    }
}
