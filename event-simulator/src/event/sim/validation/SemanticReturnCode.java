package event.sim.validation;

public enum SemanticReturnCode {
    SUCCESS(){
        @Override
        public boolean success() {
            return true;
        }
    },
    INVALID_STATE_TRANSITION(),
    SELECTED_EVENT_PERSISTENCE();

    public boolean success() {
        return false;
    }
}
