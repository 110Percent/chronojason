package event.sim.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActionChecker {
    private static final Pattern ACTION_PATTERN = Pattern.compile("<(.+),intention \\d+:((?:.|\\s)+),(?:true|false)>$");

    public static boolean actionsEqual(String action1, String action2) {
        Matcher matcher1 = ACTION_PATTERN.matcher(action1);
        Matcher matcher2 = ACTION_PATTERN.matcher(action2);

        if (!matcher1.matches() || !matcher2.matches()) {
            return false;
        }

        if (!matcher1.group(1).equals(matcher2.group(1))) {
            return false;
        }

        return IntentionChecker.intentionsEqual(matcher1.group(2).trim(), matcher2.group(2).trim());
    }
}
